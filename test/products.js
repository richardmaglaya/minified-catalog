var mongoose = require('mongoose');
var assert = require('assert');
var ProductModel = require('../models/products');
var CONFIG = { 
	PORT:  process.env.PORT || 3000,
	DB_HOST : 'mongodb://127.0.0.1/catalogue',
	DEV_USER : 'catalogue',
	DEV_PASS : 'catalogue123'
};
var constants = require('../config/constants');

mongoose.connect(CONFIG.DB_HOST, function onMongooseError(err){
	if (err instanceof Error) throw err;
		console.log('Connected to db');
});

var product_model = new ProductModel(mongoose, constants);
var newProduct = {
	name: "Sample product1",
	desc: "Sample product description here and somethine here also1",
	price: 12,
	own_by_id :3,
	category_id : 2
};

// test the create product model
product_model.create(newProduct, function onSaveErr(err, product){
	assert.ifError(err);
	assert.equal(product.name, newProduct.name, "Product name is not equal");
	assert.equal(product.desc, newProduct.desc, "Product name is not equal");
	assert.equal(product.price, newProduct.price, "Product name is not equal");
	assert.equal(product.own_by_id, newProduct.own_by_id, "Owner id is not equal");
	assert.equal(product.category_id, newProduct.category_id, "Category id id is not equal");
})

// test the read method 
