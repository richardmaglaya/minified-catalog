var casper = require('casper').create();

casper.start('http://casperjs.org/', function() {
    this.echo(this.gettitle());
});

casper.thenOpen('http://phantomjs.org', function() {
    this.echo(this.getTitle());
});

casper.run();
