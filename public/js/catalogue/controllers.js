//-- handles on the navigation Add Product and Add Category
controllers.NavigationCtrl = function($scope, $modal){
	$scope.productModal = function () {
		var modalInstance = $modal.open({
			templateUrl: '/js/catalogue/partials/product_modal.html',
			controller: controllers.ProductModalInstanceCtrl,
			resolve: { 
				product: function() {
					return false;
				}
			}
		});
	}

	$scope.categoryModal = function () {
		var modalInstance = $modal.open({
			templateUrl: '/js/catalogue/partials/category_modal.html',
			controller: controllers.CategoryModalInstanceCtrl,
			resolve: { 
				category: function() {
					return false;
				}
			}
		});
	}
};

//-- Controller for handling Product modal
controllers.ProductModalInstanceCtrl = function ($scope, $modalInstance, ProductFactory, CategoryFactory, product, filepicker, _) {

	$scope.model = {};
	$scope.model.images = [];
	
	filepicker.setKey('removed!');
	if(typeof product == "object"){
		$scope.model.name = product.name;
		$scope.model.description = product.desc;
		$scope.model.price = product.price;
		$scope.model.status = product.status;
		$scope.model.code = product.code;		
		$scope.model.images = product.images;
		$scope.model.isEdited = true;
	}
	//-- this event triggered when save button is being click on  product modal
	$scope.saveProduct = function () {
		var ids = $scope.model.category_id.split('-'),
			index = ids[0],
			id = ids[1];

		var newProduct = {
			own_by_id: 2,
			category_id : id,
			products : {
				name: $scope.model.name || '',
				desc: $scope.model.description || '',
				price: $scope.model.price || '',
				status: $scope.model.status || '',
				code: $scope.model.code || '',
				images: $scope.model.images || []
			}			
		};
		//-- for creating a new product
		if(!product){
			ProductFactory.createProduct(newProduct, function(result){
				if(result.success){
					console.log('product created');
					newProduct.products._id = result.data._id
					ProductFactory.setForBroadcast(newProduct, $scope.model.category_id); }
			});
		} else { //-- editing a product
			ProductFactory.updateProduct(product._id, newProduct, function(result){
				if(result.row){
					newProduct.products._id = product._id;
					ProductFactory.setBroadcastForUpdate(newProduct);
				}	
			});
		}

		$modalInstance.close();
	};

	//-- fetch all the categories to display on the select element
	CategoryFactory.getCategories(2, function(data){
		$scope.categories = [];
		angular.forEach(data, function(value, key){
			if(value._id == product.category_id){
				$scope.categories.push({name: value.name, _id: value._id, selected: "selected"});
			} else {
				$scope.categories.push({name: value.name, _id: value._id, selected: ""});
			}
		});
	});

	//-- event is triggered when upload button is bing clicked. It handles the filepicker api
	$scope.chooseImage = function(){
		filepicker.pickMultiple({
			mimetypes: ['image/*'],
			container: 'modal',
			services:['COMPUTER'],
			folders: true
		  },

		  function onSuccess(InkBlob){
		  	angular.forEach(InkBlob, function(value, key){
		  		var imageid = new Date().getTime();
				$scope.model.images.push({filename:value.filename, url:value.url, image_id: imageid});		  		
		  	});
		  	$scope.$apply();
		  }
		);		
	}

	//-- deletes an image
	$scope.deleteImage = function(index){
		var deleteThisImage = $scope.model.images[index];
		ProductFactory.deleteImage(product._id, product.category_id, deleteThisImage.image_id, function(result){
			if(result.success){
				filepicker.remove({url: deleteThisImage.url, filename: deleteThisImage.filename, mimetype:['image/*']}, {}, 
					function(){
						$scope.model.images.splice(index, 1);
						$scope.$apply();
					},
					function(FPError){
						console.log(FPError);
				});				
			}
		});	

	}

	//-- closes the product modal
	$scope.close = function () {
		$modalInstance.dismiss('cancel');
	};
};

//-- Controller for handling Category modal
controllers.CategoryModalInstanceCtrl = function ($scope, $modalInstance, CategoryFactory, category, _) {
	$scope.model = {};
	$scope.model.category_name = (!category) ? '' : category.name;
	$scope.isDuplicate = false;

	CategoryFactory.getCategories(2, function(data){
		$scope.categories = data;
	});

	$scope.checkDuplicate = function(){
		var category = _.findWhere($scope.categories, {name: $scope.model.category_name});
		if(category){
			$scope.isDuplicate = true;
		} else {
			$scope.isDuplicate = false;
		}
	}

	//-- adding Category
	$scope.saveCategory = function () {
		var newCategory = {
			name : $scope.model.category_name,
			own_by_id: 2
		}

		//-- Calls the Catalogue factory to create a category
		//-- it will push to the $scope.categories arrayz
		if(!category){
			CategoryFactory.createCategory(newCategory, function(result){
				if(result.success) {
					CategoryFactory.setForBroadcast(result.data);
				}
			});
		} else { // edits category name
			newCategory._id = category._id;
			CategoryFactory.updateCategory(newCategory, function(result){
				if(result.success){
					CategoryFactory.setBroadcastForUpdate(result.data);
				}	
			});
		}
		$modalInstance.close();
	};

	//-- closes the category modal
	$scope.close = function () {
		$modalInstance.dismiss('cancel');
	};
};

//-- Controller for handling the Confirmation dialog for both product and catalog modal
controllers.ConfirmationModalInstanceCtrl = function($scope, $modalInstance, CategoryFactory, ProductFactory, data){
	$scope.model = {};
	$scope.model.title = data.title; 
	$scope.model.message = data.message; 

	$scope.delete = function () {
		//-- delete product if isProduct is true
		if(data.isProduct){
			console.log('deleting Product');
			ProductFactory.deleteProduct(data.id, data.category_id, function(result){
				if(result.success){
					console.log('Product Deleted');
					ProductFactory.setBroadcastProductDeleted(data);
				}
			});
		} else { // delete category
			CategoryFactory.deleteCategory(data.id, function(result){
				if(result.success){
					CategoryFactory.setBroadcastForDelete(data.index);
				}
			});
		}

		$modalInstance.dismiss('cancel');
	};

	$scope.close = function () {
		$modalInstance.dismiss('cancel');
	};
}
//-- Controller for Catalogue this invoke on initial page
controllers.CatalogueCtrl = function($scope, $modal, CategoryFactory, ProductFactory, _){
	// gets all the category of a specific user
	// returns an array of Objects
	CategoryFactory.getCategories(2, function(data) {
		if(data.length == 0){
			$scope.categories = [];
		} else {
			$scope.categories = data;
		}
		console.log(data);
	});

	//-- fetch all the products from the server of a specific user
	//-- returns an array of Objects
	ProductFactory.getProducts(2, function (data) {
		$scope.products = [];
		angular.forEach($scope.categories, function(value, key){
			var find = _.findWhere(data, {category_id : value._id});
				if (!find) {
					$scope.products.push({own_by_id : value.own_by_id, category_id: value._id, products:[]});
				} else {
					$scope.products.push({own_by_id : find.own_by_id, category_id: find.category_id, products: find.products});
				}
		});
		console.log($scope.products);
	});

	//-- edits the category and it will show a product modal
	$scope.editCategory = function(index){
		var categorySelected = $scope.categories[index];
		var modalInstance = $modal.open({
			templateUrl: '/js/catalogue/partials/category_modal.html',
			controller: controllers.CategoryModalInstanceCtrl,
			resolve: { 
				category: function() {
					return categorySelected;
				}
			}
		});
	}
	//-- delete a specific category and it will a category modal
	$scope.deleteCategory = function(index, categoryName, categoryId) {
		var modalInstance = $modal.open({
			templateUrl: '/js/catalogue/partials/confirmation.html',
			controller: controllers.ConfirmationModalInstanceCtrl,
			resolve: { 
				data: function() {
					return {	title: 'Category', 
								message:'Are you sure you want to delete ' + categoryName +' category ?',
								id: categoryId,
								index : index,
								isProduct : false
							};
				}
			}
		});		
	}
	
	//-- edits a selected product and display a product modal
	$scope.editProduct = function(productId, categoryId,  index){
		var match = _.find($scope.products, function(product) {
			return product.category_id == categoryId;
		});

		var productSelected = $scope.products[index];
		var modalInstance = $modal.open({
			templateUrl: '/js/catalogue/partials/product_modal.html',
			controller: controllers.ProductModalInstanceCtrl,
			resolve: { 
				product: function() {
					match.products[index]._id = productId;
					match.products[index].index = index;
					match.products[index].category_id = categoryId;
					return match.products[index];
				}
			}
		});
	}
	//-- deleles a selected product with confirmation dialog
	$scope.deleteProduct = function(productId, categoryId, index) {
		var modalInstance = $modal.open({
			templateUrl: '/js/catalogue/partials/confirmation.html',
			controller: controllers.ConfirmationModalInstanceCtrl,
			resolve: { 
				data: function() {
					return {	title: 'Product', 
								message:'Are you sure you want to delete this product ? ', 
								isProduct: true,
								id: productId,
								category_id:categoryId,
								index: index
							};
				}
			}
		});		
	}

	//-- it listens when new category is being created
	$scope.$on('HandleBroadcastCategory', function () {
		var newCategory = CategoryFactory.category
		$scope.categories.push(newCategory);
		$scope.products.push({own_by_id: newCategory.own_by_id, category_id: newCategory._id, products:[]});
	});

	//-- it listens when a selected category is being edited
	$scope.$on('HandleBroadcastCategoryUpdate', function() {
		var updateCategory = CategoryFactory.updated_category;
		var match = _.find($scope.categories, function(item) {
			return item._id == updateCategory._id; 
		});

		if(match){
			match.name = updateCategory.name;
		}

	});

	//-- it listens when new product is being created
	$scope.$on('HandleBroadcastProduct', function () {
		var ids = ProductFactory.ids.split('-'),
			index = ids[0],
			id = ids[1];
		$scope.products[index].products.push(ProductFactory.products.products);
		console.log($scope.products);
	});

	//-- it listens when a selected product is being edited
	$scope.$on('HandleBroadcastProductUpdate', function() {
		var updatedProduct = ProductFactory.updated_product;
		console.log('Updated');
		console.log(updatedProduct);
		var match = _.find($scope.products, function(catalogue){
				return catalogue.category_id == updatedProduct.category_id;
		});

		console.log('Found Match');
		console.log(match);

		var product = _.find(match.products, function(product){
			return product._id == updatedProduct.products._id;
		});
		console.log('Found product');
		console.log(product);
		if(product){
			product.name = updatedProduct.products.name;
			product.desc = updatedProduct.products.desc;
			product.price = updatedProduct.products.price;
			product.code = updatedProduct.products.code;
			product.status = updatedProduct.products.status;
		}
	});

	//-- it listens when selected category is being deleted
	$scope.$on('HandleBroadcastCategoryDelete', function () {
		var index = CategoryFactory.deleted_category_index;
		$scope.categories.splice(index, 1);	
		$scope.products.splice(index, 1);
		console.log($scope.products);	
	});	

	//-- it listens when selected product is being deleted
	$scope.$on('HandleBroadcastProductDeleted', function () {
		console.log('Listening on delete product');
		var deletedProductCataloue = ProductFactory.deleted_product;
		var match = _.find($scope.products, function(catalogue){
			return catalogue.category_id == deletedProductCataloue.category_id;
		});

		if(match){
			match.products.splice(deletedProductCataloue.index, 1);
		}	
	});

}

//-- assign the module controllers
catalogueApp.controller(controllers);


