//-- creates an angular factory of angular js
catalogueApp.factory('ProductFactory', function ($http, $rootScope) {

	var ProductFactory = {};
	ProductFactory.products = [];
	ProductFactory.ids = [];
	ProductFactory.updated_product = {};
	ProductFactory.deleted_product = {};

	//-- fetch products to the server by using a http request
	ProductFactory.getProducts = function (account_id, callback) {
		$http.get('/products/list/' + account_id).success(function(data, status, headers, config){
			//console.log(data);
			return callback(data);
		});
	}
	//-- create a product and send a post to the server by using a http request
	ProductFactory.createProduct = function (data, callback) {
		$http.post('/products/create', data).success(function(data, status, headers, config){
			return callback(data);
		})
	}
	//-- updates product and send a put request to the server
	ProductFactory.updateProduct = function (id, data, callback) {
		$http.put('/products/update/' + id, data.products, {method:'PUT'}).success(function(result, status, headers, config){
			return callback(result);
		});
	}
	//-- deletes a product and send a get request to the server
	ProductFactory.deleteProduct = function (id, category_id, callback) {
		$http.get('/products/delete/'+ id +'/'+ category_id).success(function(result, status, headers, config){
			return callback(result);
		});
	}
	//-- assigned the delete data to be broadcast
	ProductFactory.setBroadcastProductDeleted = function (data){
		this.deleted_product = data;
		this.broadcastDeletedProduct();
	}
	//-- broadcast an event if there is product is being deleted
	ProductFactory.broadcastDeletedProduct = function() {
		$rootScope.$broadcast('HandleBroadcastProductDeleted');
	}

	//-- assgin a data and ids to broadcast
	ProductFactory.setForBroadcast = function (data, ids) {
		this.products = data;
		this.ids = ids;
		this.broadNewProduct();
	}
	//-- broadcast an event to be listen of new product is being created
	ProductFactory.broadNewProduct = function(){
 		$rootScope.$broadcast('HandleBroadcastProduct');
	}

	//-- assign an updated value for broadcast
	ProductFactory.setBroadcastForUpdate = function (data){
		this.updated_product = data;
		this.broadcastUpdateProduct();
	}

	//-- broadcast an event to be listen if there is an update on the product
	ProductFactory.broadcastUpdateProduct = function(){
 		$rootScope.$broadcast('HandleBroadcastProductUpdate');
	}

	//-- deletes a product image and send a get request to the server
	ProductFactory.deleteImage = function (id, category_id, imageid, callback) {
		$http.get('/products/delete/image/'+ id +'/'+ category_id+'/' + imageid).success(function(result, status, headers, config){
			return callback(result);
		});
	}

	//-- return factory
	return ProductFactory;

});

//-- creates a CategoryFactory to be used on the controller
//-- Category CRUD functionality
catalogueApp.factory('CategoryFactory', function ($http, $rootScope) {
	var CategoryFactory = {};
	CategoryFactory.category = [];
	CategoryFactory.updated_category = {};
	CategoryFactory.deleted_category_index = {};
	CategoryFactory.categories = [];
	
	//-- create a category using http post request to the server
	CategoryFactory.createCategory = function (data, callback) {
		$http.post('/category/create', data).success(function(data, status, headers, config){
			return callback(data);
		});
	}
	//-- fetch all the categories using get request to the server
	CategoryFactory.getCategories = function (account_id, callback){
		$http.get('/category/list/' + account_id).success(function(data, status, headers, config){
			return callback(data);
		});
	}

	//-- send a post request and updates the category name
	CategoryFactory.updateCategory = function (data, callback){
		$http.post('/category/update', data).success(function(data, status, headers, config) {
			return callback(data);
		});
	}

	//-- deletes specific category and send a get request
	CategoryFactory.deleteCategory = function (id, callback){
		$http.get('/category/delete/' + id).success(function(data, status, headers, config) {
			return callback(data);
		});
	}

	//-- assign a newly created category
	CategoryFactory.setForBroadcast	= function (data) {
		this.category = data;
		this.broadcastNewCategory();
	}

	//-- send a broadcast event to listen when a new category is created
	CategoryFactory.broadcastNewCategory = function(){
 		$rootScope.$broadcast('HandleBroadcastCategory');
	}

	//-- assign an updated category
	CategoryFactory.setBroadcastForUpdate = function (data){
		this.updated_category = data;
		this.broadcastUpdateCategory();
	}

	//-- send a broadcast event on every category edited
	CategoryFactory.broadcastUpdateCategory = function(){
 		$rootScope.$broadcast('HandleBroadcastCategoryUpdate');
	}

	//-- assign a delete category index
	CategoryFactory.setBroadcastForDelete = function (index){
		this.deleted_category_index = index;
		this.broadcastDeleteCategory();
	}	

	//-- sends a broadcast event for a deleted category
	CategoryFactory.broadcastDeleteCategory = function(){
 		$rootScope.$broadcast('HandleBroadcastCategoryDelete');
	}	

	return CategoryFactory;
});

//-- create a underscore utility factory
catalogueApp.factory('_', function() {
	return window._;
});

//-- create a filepicker factory to be on uploading images
catalogueApp.factory('filepicker', function() {
	return window.filepicker;
});