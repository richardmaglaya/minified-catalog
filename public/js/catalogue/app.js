var catalogueApp = angular.module('catalogueApp', ['ui.bootstrap']);
var controllers = {};

catalogueApp.config(function($routeProvider){
	$routeProvider
		.when('/', { templateUrl: '/js/catalogue/partials/page.html', controller:'CatalogueCtrl' });
});
