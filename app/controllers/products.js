/**
 * Product Router 	Manages the incoming request of the products
 * @param ProductModel In-charge on manipulating the products to the database
 */
module.exports = function (productModel, constants){

	var self = this;
	
	//-- handles on creating a product
	self.create = function (req, res){
	console.log(req.body);
		productModel.create(req.body, function(err, product){
			if(err instanceof Error){
				res.json({success: false});		
			} else {
				res.json({success: true, data: product});
			}
		});		
	};

	//-- handles on deleting a product by id
	self.delete = function(req, res){
		var id = req.params.id;
		var categoryId = req.params.category_id;
		productModel.delete(id, categoryId, function(err, data){
			if(err instanceof Error){
				res.json({success: false});		
			} else {
				res.json({success: true, data: data});
			}
		});	
	};

	//-- handles on getting the information of the product by id
	self.read = function(req, res){
		var id = req.params.id;
		productModel.read(id, function(err, product){
			if(err instanceof Error){
				res.statusCode = 500;
				res.end(constants.SERVERERR);			
			} else {
				res.send(product);
			}			
		});
	};

	//-- handles on updating the product by id
	self.update = function(req, res){
		var id = req.params.id;
		productModel.update(id, req.body, function(err, num_rows){
			if(err instanceof Error){
				console.log(err);
				res.statusCode = 500;
				res.end(constants.SERVERERR);			
			} else {
				res.json({row: num_rows});
			}			
		});
	};

	//-- handles on fetching the list of products
	self.list = function (req, res){
		var account_id = req.params.account_id;
		productModel.list(account_id, function(err, products){
			if(err instanceof Error){
				res.statusCode = 500;
				res.end('something is wrong');	
			} else {
				res.send(products);
			}
		});
	};

	//-- deletes image of the products
	self.deleteImage = function (req, res){
		var imageObj = { id : req.params.id, categoryid : req.params.categoryid, imageid:req.params.imageid } ;
		productModel.deleteImage(imageObj,  function(err, numAffected){
			if(err instanceof Error){
				res.statusCode = 500;
				res.end('something is wrong');	
			} else {
				res.send({success: true, data: numAffected});
			}
		});
	};
}