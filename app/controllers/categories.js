/**
 * Product Router 	Manages the incoming request of the products
 * @param ProductModel In-charge on manipulating the products to the database
 */
module.exports = function (categoryModel, constants){

	var self = this;

	self.create = function (req, res){
		categoryModel.create(req.body, function(err, category){
			if (err instanceof Error) {
				res.json({success: false});		
			} else {
				res.json({success: true, data: category});
			}
		});		
	};

	self.delete = function(req, res){
		var id = req.params.id;
		categoryModel.delete(id, function(err, data) {
			if(err instanceof Error){
				res.json({success: false});		
			} else {
				res.json({success: true, data: data});
			}
		});
	};

	self.read = function(req, res){

	};

	self.update = function(req, res){
		categoryModel.update(req.body, function(err, data){
			if(err instanceof Error){
				res.json({success: false});		
			} else {
				res.json({success: true, data: data});
			}
		});
	};

	self.list = function (req, res){
		var id = req.params.account_id;
		categoryModel.list(id, function(err, categories){
			if(err instanceof Error){
				res.json({success: false});		
			} else {
				res.send(categories);
			}
		});
	};
}