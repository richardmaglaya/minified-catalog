module.exports = function(app, mongoose, _){

	//-- instatiate routes
	var ProductsRoutes = require('./products'),
		CategoryRoutes = require('./categories'),
		ProductsModel = require('./../models/products'),
		CategoryModel = require('./../models/category');

	//-- instantiate model
	var	category_model = new CategoryModel(mongoose),
		product_model = new ProductsModel(mongoose, category_model, _);

	//-- instantiate model
	var pRoutes = new ProductsRoutes(product_model),
		cRoutes = new CategoryRoutes(category_model);

	//-- render an index file this can found on the views
	app.get('/', function(req, res){
		res.render('index', {});
	});

	//-- Product routes
	app.post('/products/create', pRoutes.create);
	app.get('/products/read', pRoutes.read);
	app.put('/products/update/:id', pRoutes.update);
	app.get('/products/delete/:id/:category_id', pRoutes.delete);
	app.get('/products/list/:account_id', pRoutes.list);
	app.get('/products/delete/image/:id/:categoryid/:imageid', pRoutes.deleteImage);

	//-- Category routes
	app.post('/category/create', cRoutes.create);
	app.get('/category/read', cRoutes.read);
	app.post('/category/update', cRoutes.update);
	app.get('/category/delete/:id', cRoutes.delete);
	app.get('/category/list/:account_id', cRoutes.list);	
}