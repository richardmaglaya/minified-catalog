/** @module models/product.js   
 * Product Models - in-charge on managing the product
 */
module.exports = function(mongoose, categoryModel, _){

	// Construct schema for product collection
	var ProductsSchema = new mongoose.Schema({
		 _id: Number,
		own_by_id: Number,
		category_id: mongoose.Schema.ObjectId,
		products: [{
			name: String, 
			imageid: String, 
			desc: String,
			price: Number, 
			added_date: { type: Date, default: Date.now },			
			status: Number,
			code: String,
			is_active: {type: Boolean, default: true },
			images: [
				{filename: String, url: String, image_id: Number}
			]
		}],
	});
	

	var Product = mongoose.model('Products', ProductsSchema);
	var self = this;

	/** Handles on inserting the data to the collection
	 * @inner
	 * @param {object} data - Data to be inserted
	 * @param {function} callback - A function
 	 * @returns {function} callback - Return the callback function
	 */
	self.create = function(data, callback) {		
		Product.findOneAndUpdate(
					{ own_by_id : data.own_by_id, category_id: data.category_id}, 
					{ $push : { products: data.products} },
					{ new: true, upsert: true}, 
					function onSave(err, doc){
						if(err){
							return callback(err, false);
						} else {
							var found = _.findWhere(doc.products, {name : data.products.name});
							return callback(null, found);
						}
					});
	};

	/** Handles on getting the data from the collection
	 * @inner
	 * @param {Number} id - Use as identifier to find the single record
	 * @param {function} callback - A function
 	 * @returns {function} callback - Return the callback function
	 */
	self.read = function(id, callback){
		// fail-fast if id was specified
		if(!id || id < 1){
			return callback(new Error('No id specified'));
		}

		Product.findOne({id: id}, function onFindOne(err, product){
			if(err) { 
				callback(err, false); 
			} else {  			
				callback(null, product);
			}
		});
	};

	/** Handles on updating the data to the collection
	 * @inner
	 * @param {Number} id - Represent as identifier to update record
	 * @param {object} data - A JSON Represent as the data to be updated
	 * @param {function} callback - A function
	 * @returns {function} callback - Return the callback function
	 */
	self.update = function(id, data, callback){
		// fail-fast if no id was specified
		if(!id || id < 1){
			return callback(new Error('No id specified'));
		}

		Product.update(	{ "products._id":id }, 
						{ 
							$set : {	
								"products.$.name": data.name, "products.$.desc" : data.desc, 
								"products.$.price": data.price, "products.$.status" : data.status, 
								"products.$.code": data.code,
								"products.$.images": data.images
							}
						}, 
						{}, 
						function(err, numberAffected, raw){
							if(err) { 
								return callback(err);
							} else {
								return callback(null, numberAffected);
							}						
						} );
	};

	/** Handles on deleting the data from the collection
	 * @inner
	 * @param {Number} id -	Represent as identifier to be deleted 
	 * @param {Number} category_id - Represent category_id as identifier to be deleted record
	 * @param {function} callback - A function
	 * @returns {function} callback - Return the callback function
	 */
	self.delete = function(id, category_id, callback){
		Product.update({category_id : category_id}, {$pull : { "products" : {_id: id}}}, function(err, numberAffected, raw) {
			if(err){
				return callback(err, false);
			} else {
				return callback(null, raw);
			}
		});
	}	

	/** Handles on getting all the products by account owner id
	 * @inner
	 * @param {Number} account_id -	Represent as identifier to be fetch in the collection
	 * @param {function} callback -	A function
	 * @returns {function} callback - Return the callback function
	 */
	self.list = function(account_id, callback) {
		// fetch all the products by account_owner_id
		var categoryIds = [];
		categoryModel.ActiveCategoryListId(account_id, function(err, data){
			categoryIds = _.values(data);
			Product.find({own_by_id : account_id, category_id: {$in: categoryIds}}, function(err, products){
				if(err){
					return callback(err, false);
				} else {
					return callback(null, products);
				}	
			});
		});
	};

	self.deleteImage = function(imageObj, callback){
		Product.update({category_id: imageObj.categoryid, products:{$elemMatch:{_id: imageObj.id}}}, {$pull: {"products.$.images":{image_id:imageObj.imageid} }},  function(err, numberAffected, raw) {
			if(err){
				return callback(err, false);
			} else {
				return callback(null, numberAffected);
			}
		});
	}
}