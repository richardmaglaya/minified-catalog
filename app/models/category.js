/** @module models/category.js 
 * Manages the category CRUD functionality
 */
module.exports = function(mongoose){

	var CategoriesSchema = new mongoose.Schema({
		name: String,
		own_by_id: Number,
		category_id : mongoose.Schema.ObjectId,
		is_active : {type: Boolean, default: true}
	});

	var Category = mongoose.model('Categories', CategoriesSchema);
	var self = this;

	/** Creates category and return the created category 
	 * @inner
	 * @param {object} data - A JSON object that contain data to be saved
	 * @param {function} callback - A function
	 * @returns {function} callback - Return the callback function
	 */
	self.create = function(data, callback){
		var category = new Category();
		category.category_id = data.category_id;
		category.own_by_id = data.own_by_id;
		category.name = data.name;
		category.save(function onSave(err, data){
			if(err){
				return callback(err, false);
			} else {
				return callback(null, data);
			}
		});
	}

	/** Updates category and return the updated category 
	 * @inner
	 * @param {object} data - A JSON object that contain data to be updated
	 * @param {function} callback - A function
	 * @returns {function} callback - Return the callback function
	 */
	self.update = function(data, callback) {
		Category.findByIdAndUpdate(data._id, {name: data.name}, function(err, data){
			if(err){
				return callback(err, false);
			} else {
				return callback(null, data);
			}
		});
	}

	/** Fetch all categories from db
	 * @inner
	 * @param {Number} account_id - Represents the account owner id
	 * @param {function} callback - A function
	 * @returns {function} callback - All categories
	 */
	self.list = function(account_id, callback) {
		Category.find({own_by_id : account_id, is_active : true}, function(err, categories){
			if(err) {
				return callback(err, false);
			} else {
				return callback(null, categories);
			}	
		});
	};	

	/** Fetch all active categories from db
	 * @inner
	 * @param {Number} account_id - Represents the account owner id
	 * @param {function} callback - A function
	 * @returns {function} callback - All categories
	 */
	self.ActiveCategoryListId = function(account_id, callback) {
		Category.find({own_by_id : account_id, is_active : true},{_id: 1}, function(err, categories){
			if(err) {
				return callback(err, false);
			} else {
				return callback(null, categories);
			}	
		});
	};	

	/** Mark an specific category to be archived using id 
	 * @inner
	 * @param {Number} id - Represents the unique id of the category id
	 * @param {function} callback - A function
	 * @returns {function} callback - The deleted category
	 */
	self.delete = function(id, callback) {
		Category.findByIdAndUpdate(id, {is_active: false}, function(err, data){
			if(err){
				return callback(err, false);
			} else {
				return callback(null, data);
			}
		});
	}	

}