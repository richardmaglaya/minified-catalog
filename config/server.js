module.exports = function(app, mongoose, express, path){
	
	// for development configuration of the express middleware
	app.configure('development',function () {
		app.set('db', 'mongodb://127.0.0.1/catalogue');
		app.set('port', process.env.PORT || 3000) 
		app.set('view engine', 'ejs'); 
		app.set('views', 'app/views/');
		app.use(express.logger('dev'));
		app.use(express.cookieParser());		
		app.use(express.bodyParser());
		app.use(express.methodOverride());
		app.enable('trust proxy');
		app.use(app.router);	
		app.use(express.static('public/'));
		app.use(express.session({ secret: 'catalogue' }));
	});		

	// for product configuration of the middleware
	app.configure('production', function(){
		app.set('db', 'mongodb://127.0.0.1/catalogue');
		app.set('port', process.env.PORT || 3000); 
		app.set('view engine', 'ejs'); 
		app.set('views', 'app/views/');		
		app.use(express.bodyParser());
		app.use(express.cookieParser());		
		app.use(express.methodOverride());
		app.enable('trust proxy');
		app.use(app.router);	
		app.use(express.static('public/'));
		app.use(express.session({ secret: 'catalogue' }));		
	});	

	// connect to mongodb
	mongoose.connect(app.get('db'), function onMongooseError(err){
		if (err instanceof Error) throw err;
			console.log('Connected to db');
	});

}